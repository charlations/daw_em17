function verificaContra() {
    if (document.getElementById("pass").value !== "" && document.getElementById("verpass").value !== "") {
        var res = document.getElementById("resverificador");
        if (document.getElementById("pass").value === document.getElementById("verpass").value) {
            res.innerHTML = "<p>¡Contraseña verificada!</p>";
        } else {
            res.innerHTML = "<p>Las contraseñas no coinciden</p>";
        }
    } else {
        window.alert("Escriba ámbas contraseñas");
    }
}
document.getElementById("butver").onclick = verificaContra;

function creaCarrito() {
    var resul = document.getElementById("rescompra");
    var cbotwz = document.getElementById("botwz").value;
    var cbotwl = document.getElementById("botwl").value;
    var cwwz = document.getElementById("wwz").value;
    
    if (cbotwz < 0 || cbotwl < 0 || cwwz < 0) {
        resul.innerHTML = "<h6>Error en las cantidades</h6>";
    } else if (cbotwz != 0 || cbotwl != 0 || cwwz != 0) {
        cbotwz *= 1;
        cbotwl *= 1;
        cwwz *= 1;
        var cantidad = (cbotwl+cbotwz+cwwz);
        var preventa = (cbotwz*400)+(cbotwl*400)+(cwwz*300);
        var iva = preventa * .15;
        resul.innerHTML = "<p><h6>Artículos Seleccionados: </h6> " + cantidad + "</p>"
        + "<p><h6>Pretotal: </h6>$" + preventa + "</p>"
        + "<p><h6>Iva: </h6>$" + iva + "</p>"
        + "<p><h6>Cuenta Total: </h6>$" + (preventa + iva) + "</p>";
    } else {
        resul.innerHTML = "<h6>Carrito vacío</h6>";
    }
}
document.getElementById("butcomp").onclick = creaCarrito;
function enviaDatos(){
    var resultados = document.getElementById("resinscripcion");
    var nombre = document.getElementById("nom").value;
    var dir = document.getElementById("address").value;
    var dir2 = document.getElementById("address2").value;
    var mail = document.getElementById("email").value;
    if (validatodos(nombre, dir, dir2, mail)) {
        resultados.innerHTML = "<h3>¡Gracias " + nombre + "!</h3> <p>Se enviarán correos periódicamente a " + mail +" con tiendas y ofertas cerca de " + dir + " y en el área de " + dir2 + "</p>";
    } else {
        resultados.innerHTML = "<h5>Error en los datos ingresados</h5>"
    }
}
document.getElementById("butenvia").onclick = enviaDatos;
function validateEmail(email){
    var re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return re.test(email);
}
function validateString(str){
    return (typeof str == "string");
}
function validatodos(nom, ad, add, em){
    return (validateEmail(em) && validateString(nom) && validateString(ad) && validateString(add));
}

/*document.miForma.go.value = "clic me";

function imprimeElegidos() {
    var numElegidos = 0;
    var dom = document.getElementById("deportes");
    for (index = 0; index < dom.opciones.length; index++) {
        if (dom.opciones[index].checked) {
            numElegidos++;
        }
    }
    var elegidos = document.getElementById("elegidos");
    elegidos.innerHTML = "Elegiste " + numElegidos + " deportes.";
}

function deshabilitar() {
    document.miForma.go.disabled = true;
}

document.getElementById("go").onclick = deshabilitar;*/
